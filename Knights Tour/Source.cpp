#include<iostream>
#include<array>
using namespace std;

// Handle a Move as a separate "Thing".
typedef struct Move {
	int x, y;

}Move;

bool isMovePossible(Move next_move, int **params, int limit) {
	int i = next_move.x;
	int j = next_move.y;
	if ((i >= 0 && i < limit) && (j >= 0 && j < limit) && (params[i][j] == 0))
		return true;
	return false;
}

bool TakeTheTour(int **param, Move possibleMoves[], Move current_position, int amountOfMoves, int limit)
{
	int i;
	Move next_move;
	// The knight has visited all cells if the amount of moves are equal to the board size (limit * limit) minus startposition (-1)
	if (amountOfMoves == limit * limit - 1) {
		return true;
	}

	// Try all the possible moves from where the knight currently is.
	for (i = 0; i < limit; i++) {

		// update what the next move will be.
		next_move.x = current_position.x + possibleMoves[i].x;
		next_move.y = current_position.y + possibleMoves[i].y;

		// if the move is possible, increase the move count and store it in tour matrix
		if (isMovePossible(next_move, param, limit)) {

			// Take the board values
			param[next_move.x][next_move.y] = amountOfMoves + 1;
			if (TakeTheTour(param, possibleMoves, next_move, amountOfMoves + 1, limit) == true) {
				return true;
			}
			else {
				// this move was invalid, try out other possiblities 
				param[next_move.x][next_move.y] = 0;
			}
		}
	}
	return false;
}

void Initialize()
{
	int userValue;

	// Only accept boardSizes bigger than 4.
	do {
		cout << "\n Insert preferable boardSize (>= 5): ";

		// Ask user value
		cin >> userValue;

	} while (userValue <= 4);

	// Make a 2 dimensional array of pointers which points to an int.
	int** ary = new int*[userValue];
	for (int i = 0; i < userValue; ++i)
		ary[i] = new int[userValue];

	// fill the array with 0's.
	for (int i = 0; i < userValue; ++i)
		for (int j = 0; j < userValue; ++j)
			ary[i][j] = 0;


	// Make a new instance of Move which holds every legal move of the knight.
	Move legal_moves[8] = { { 2,1 },{ 1,2 },{ -1,2 },{ -2,1 },
	{ -2,-1 },{ -1,-2 },{ 1,-2 },{ 2,-1 } };

	// Start from scratch (0,0) on the board.
	Move startPos = { 0, 0 };

	if (TakeTheTour(ary, legal_moves, startPos, 0, userValue) == false) {
		cout << "\nKnight tour does not exist";
	}

	else {
		cout << "\nTour exists ...\n";
		int i, j;
		for (i = 0; i < userValue; i++) {
			for (j = 0; j < userValue; j++) {
				cout << ary[i][j] << "\t";
			}
			cout << endl;
		}
	}
}

// main
int main() {
	Initialize();
	cout << endl;
	cin.ignore();
	cin.get();

}